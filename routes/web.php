<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'indexController@index');
Route::get('/register', 'AuthController@register');
Route::post('/walcome', 'AuthController@walcome');
Route::get('/data-table', 'indexController@table');

// CRUD CAST
Route::get('/cast/create', 'CastController@create'); //mengarah form tambah data
Route::post('/cast', 'CastController@store'); // menyimpan data form ke databasse
Route::get('/cast', 'CastController@index'); // ambil data ke data tampilkan di blade
Route::get('/cast/{cast_id}', 'CastController@show'); // ambil data ke data tampilkan di blade
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::get('/cast/{cast_id}', 'CastController@update'); 
Route::get('/cast/{cast_id}', 'CastController@destroy'); 

