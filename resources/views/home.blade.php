@extends('layout.master')
@section('title')
    Halaman Utama
@endsection
@section('content')
<h1>Media Online</h1>
    <h2>Sosial Media Developer</h2>
    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
    <h4>Benefit Join di Media Online</h4>
    <ul>
        <li>Mendapatkan motivasi dari sesama para Developer</li>
        <li>Sharing Knowlenge</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
    <h4>Cara Bergabung ke Media Online</h4>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
        <li>Selesai</li>
    </ol>
@endsection