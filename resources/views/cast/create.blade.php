@extends('layout.master')
@section('title')
    Halaman Tambah User
@endsection
@section('content')
<form action="/cast" method="post">
  @csrf
  <div class="form-group">
    <label>Nama Lengkap</label>
    <input type="text" class="form-control" name="nama">
  </div>
  <div class="form-group">
    <label>Umur</label>
    <input type="text" class="form-control" name="umur">
  </div>
  <div class="form-group">
    <label>Bio</label>
    <input type="text" class="form-control" name="bio">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection