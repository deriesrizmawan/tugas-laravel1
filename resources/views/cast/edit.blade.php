@extends('layout.master')
@section('title')
    Halaman Tambah User
@endsection
@section('content')
<form action="/cast/{{$cast->id}}" method="post">
  @csrf
  @method('put');
  <div class="form-group">
    <label>Nama Lengkap</label>
    <input type="text" value="{{$cast->nama}}" class="form-control" name="nama">
  </div>
  <div class="form-group">
    <label>Umur</label>
    <input type="text"  value="{{$cast->umur}}" class="form-control" name="umur">
  </div>
  <div class="form-group">
    <label>Bio</label>
    <input type="text"  value="{{$cast->bio}}" class="form-control" name="bio">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection