@extends('layout.master')
@section('title')
    Halaman List User
@endsection
@section('content')

<a href="/cast/create" class="btn btn-primary mb-3">Tambah User</a>

<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Bio</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($cast as $key => $data)
    <tr>
        <td>{{$key + 1}}</td>
        <td>{{$data->nama}}</td>
        <td>{{$data->umur}}</td>
        <td>{{$data->bio}}</td>
        <td>
            <a href="/cast/{{$data->id}}" class="btn btn-success">Detail</a>
            <a href="/cast/{cast_id}/edit" class="btn btn-info">Edit</a>

            <form action="/cast/{{$cast->id}}" method="post">
              @csrf
              @method('delete')
              <input type="submit" value="Hapus" class="btn btn-danger">
            </form>
        </td>
    </tr>
    @empty
    @endforelse
  </tbody>
</table>
@endsection