<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('register');
    }

    public function walcome(Request $request) {
        $namaDepan = $request['nama_depan'];
        $namaBelakang = $request['nama_belakang'];

        return view('walcome', compact("namaDepan", "namaBelakang"));
    }
}
